-- SQL server code for B320 SRSD ProjectID
-- Date: 11/29/19
-- Dev Status: Active (not complete)

CREATE TABLE [GroupSubmission] (
  [GroupID] int,
  [Poster] varchar(max), 
  PRIMARY KEY ([GroupID])
);

CREATE TABLE [Speaker] (
  [SpeakerID] int,
  [SpeakerName] varchar(max),
  [Description] varchar(max),
  PRIMARY KEY ([SpeakerID])
);

CREATE TABLE [Judges] (
  [JudgeID] int,
  [JudgeFName] varchar(max),
  [JudgeLName] varchar(max),
  [JudgeEmail] varchar(max),
  PRIMARY KEY ([JudgeID])
);

CREATE TABLE [Awards] (
  [ProjectID] int,
  [AwardTitle] varchar(max),
  [AwardAmount] int,
  [Category] varchar(max),
  PRIMARY KEY ([ProjectID])
);

CREATE TABLE [Group] (
  [GroupID] int,
  [MentorID] int,
  [Category] varchar(max),
  [GroupLeader] varchar(max),
  [ProjectID] int,
  PRIMARY KEY ([GroupID])
);

CREATE TABLE [GroupStudentLink] (
	[GroupID] int,
	[StudentID] int,
	[Role] varchar(max)
);

CREATE TABLE [Registration] (
  [AcademicYear] int,
  [FinalDraftDeadline] varchar(max),
  [MentorApprovalDeadline] varchar(max),
  [AbstractDeadline] varchar(max),
  [RegistrationDeadline] varchar(max),
  PRIMARY KEY ([AcademicYear])
);

CREATE TABLE [Event] (
  [EventDate] varchar(max),
  [EventLocation] varchar(max),
  [EventSchedule] varchar(max),
  [EventYear] int,
  PRIMARY KEY ([EventYear])
);

CREATE TABLE [Project] (
  [ProjectID] int,
  [Poster] varchar(max),
  [Title] varchar(max),
  [Category] varchar(max),
  [MentorID] int,
  [Abstract] varchar(max),
  [PresentationType] varchar(max),
  [AcademicYear] int,
  [PrimaryStudentID] int,
  [GroupID] int,
  PRIMARY KEY ([ProjectID])
);

CREATE TABLE [Student] (
  [StudentID] int,
  [StudentFName] varchar(max),
  [StudentLName] varchar(max),
  [StudentEmail] varchar(max),
  PRIMARY KEY ([StudentID])
);

CREATE TABLE [Sponsors] (
  [SponsorID] int,
  [SponsorName] varchar(max),
  [SponsorAmount] int,
  PRIMARY KEY ([SponsorID])
);

CREATE TABLE [Mentor] (
  [MentorID] int,
  [MentorFName] varchar(max),
  [MentorLName] varchar(max),
  [MentorEmail] varchar(max),
  [MentorDepartment] varchar(max),
  PRIMARY KEY ([MentorID])
);

/*
DROP TABLE [Mentor];
DROP TABLE [Sponsors];
DROP TABLE [Student];
DROP TABLE [Project];
DROP TABLE [Event];
DROP TABLE [Registration];
DROP TABLE [GroupStudentLink];
DROP TABLE [Group];
DROP TABLE [Awards];
DROP TABLE [Judges];
DROP TABLE [Speaker];
DROP TABLE [GroupSubmission];
*/