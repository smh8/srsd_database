﻿-- SQL server code for B320 SRSD ProjectID
-- Date: 11/29/19
-- Dev Status: Active (not complete)

INSERT INTO [Student]
VALUES 
(1001, 'Ziare', 'Joseph', 'Ziare@gmail.com'),
(1002, 'Aava', 'Kane', 'Aava@gmail.com'),
(1003, 'Jordan', 'Hill', 'JH@uscb.com'),
(1004, 'Bob', 'Dole', 'dobbob@uscb.com'),
(1005, 'Mob', 'Job', 'mobjob2@uscb.com'),
(1006, 'Bill', 'Smith', 'bsmith@gmail.com'),
(1007, 'Spoi', 'Boi', 'notspy@nsa.org'),
(1008, 'Bob', 'Kane', 'bobkane@gmail.com'),
(1009, 'Sam', 'Bork', 'sbork@uscb.com'),
(1010, 'Jane', 'Doe', 'jd@gmail.com'),
(1011, 'Maxi', 'Thor', 'mthor@thor.com'),
(1012, 'Will', 'Thor', 'wthor@thor.com'),
(1013, 'Kane', 'Kane', 'candycane@canecane.org'),
(1014, 'Davros', 'Kelad', 'daleksupremo@dalek.net'),
(1015, 'Manx', 'Electrik', 'electrik@manx.gov'),
(1016, 'Boo', 'Ghost', 'spoopu@spoopy.com')